#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

typedef struct {
    char masinhvien[10];
    char ten[50];
    char sodienthoai[20];
} sinhvien;
sinhviens[10];

void danhsachcacluachon() {
    printf("1. Them moi sinh vien\n");
    printf("2. Hien thi danh sach sinh vien\n");
    printf("3. Luu danh sach sinh vien ra file\n");
    printf("4. Doc danh sach sinh vien tu file\n");
    printf("5. Thoat chuong trinh\n");
}

void themsinhvien(sinhvien sinhviens[], int a) {
    printf("1. Them moi sinh vien.\n");
    for (int i = 0; i < a; ++i) {
        do {
            printf("Vui long nhap ma sinh vien thu %d", i + 1);
            scanf("%s", sinhviens[i].masinhvien);
            fflush(stdin);
            if (strlen(sinhviens[i].masinhvien) != 5) {
                printf("Ma sinh vien khong du 5 ky tu. Moi nhap lai:\n");
            }
        } while (strlen(sinhviens[i].masinhvien) != 5);
        fflush(stdin);

        printf("Vui long nhap ten sinh vien thu %d", i + 1);
        fgets(sinhviens[i].ten, sizeof(sinhviens[i].ten) * sizeof(char), stdin);
        strlen(sinhviens[i].ten);
        sinhviens[i].ten[strlen(sinhviens[i].ten) - 1] = ' ';
        fflush(stdin);

        printf("Vui long nhap so dien thoai sinh vien thu %d", i + 1);
        fgets(sinhviens[i].sodienthoai, sizeof(sinhviens[i].sodienthoai) * sizeof(char), stdin);
        sinhviens[i].sodienthoai[strlen(sinhviens[i].sodienthoai) - 1] = ' ';
        fflush(stdin);
    }
}

void hienthidanhsach(sinhvien sinhviens[], int a) {
    printf("Hien thi danh sach sinh vien:\n");
    printf("%-22s |%-22s |%-22s\n", "- ma sinh vien", "Ten sinh vien", "so dien thoai");
    for (int i = 0; i < a; ++i) {
        printf("-%-22s|%-22s |%-22s\n", sinhviens[i].masinhvien, sinhviens[i].ten, sinhviens[i].sodienthoai);
    }
}

void luudanhsach(const char *p, sinhvien sinhviens[], int a) {
    FILE *fp;
    fp = fopen(p, "w");
    if (fp != NULL) {
        fprintf(fp, "Hien thi danh sach sinh vien :\n");
        fprintf(fp, "- %-22s | %-22s | %-22s\n", "- ma sinh vien", "Ten sinh vien", "so dien thoai");
        for (int i = 0; i < a; ++i) {
            fprintf(fp, "%-22s | %-22s | %-22s\n", sinhviens[i].masinhvien, sinhviens[i].ten, sinhviens[i].sodienthoai);
        }
        fclose(fp);
        printf("Luu danh sach sinh vien vao: danhsach.txt\n");
    } else
        printf("Tap tin chua the mo");
}

void docdanhsach(const char *p, sinhvien sinhviens[], int a) {
    FILE *fp;
    fp = fopen(p, "r+");
    fscanf(fp, "Hien thi danh sach sinh vien :\n");
    fscanf(fp, "- %-22s | %-22s | %-30s\n", "- ma sinh vien", "Ten sinh vien", "so dien thoai");
    printf("Hien thi danh sach sinh vien :\n");
    printf("%-30s | %-30s | %-30s\n", "- ma sinh vien", "Ten sinh vien", "so dien thoai");
    for (int i = 0; i < a; ++i) {
        fscanf(fp, "%-22s| %-22s| %-22s\n", sinhviens[i].masinhvien, sinhviens[i].ten, sinhviens[i].sodienthoai);
        printf("- %-28s | %-30s | %-22s\n", sinhviens[i].masinhvien, sinhviens[i].ten, sinhviens[i].sodienthoai);

    }
    printf("\n");
    fclose(fp);

}

int main() {
    int choice, a;
    fflush(stdin);
    while (1 == 1) {
        danhsachcacluachon();
        printf(" moi lua chon:\n");
        scanf("%d", &choice);
        fflush(stdin);
        switch (choice) {
            case 1:
                do {
                    printf("Moi nhap so luong sinh vien: ");
                    scanf("%d", &a);
                    if (a <= 0 || a > 10) {
                        printf("Danh sach sinh vien day\n");
                    }
                } while (a > 10 || a <= 0);
                themsinhvien(sinhviens, a);
                break;

            case 2:
                hienthidanhsach(sinhviens, a);
                break;
            case 3:
                luudanhsach("danhsach.txt", sinhviens, a);
                break;
            case 4:
                docdanhsach("danhsach.txt", sinhviens, a);
                break;
            case 5:
                printf("5. Thoat chuong trinh");
                exit(1);
            default:
                printf(" Lua chon khong co trong danh sach! Vui long nhap lai");
                break;
        }
    }
}